class CreateServers < ActiveRecord::Migration
  def change
    create_table :servers do |t|
      t.string :title
      t.string :ip
      t.text :description
      t.string :modcomments

      t.timestamps
    end
  end
end
